package example

// GENERATED CODE, EDITS WILL BE LOST
//
// generated from example/Thing
// using http://gitlab.com/brianstarke/go-beget

import (
	"fmt"
	"strings"
)

// Operator is an operator to be used in a SearchRequest filter
type Operator int

// Enum-like helpers for Operator
const (
	Eq Operator = iota
	NotEq
	Like
	NotLike
	GreaterThan
	GreaterThanOrEq
	LesserThan
	LesserThanOrEq
	IsNull
	NotNull
)

// Condition is an indicator of whether or not a filter should
// be added as an AND or an OR to the search predicates
type Condition int

// Enum-like helpers for Condition
const (
	And Condition = iota
	Or
)

// MarshalText implements https://golang.org/pkg/encoding/#TextMarshaler
func (o Operator) MarshalText() ([]byte, error) {
	var data string

	switch o {
	case Eq:
		data = "eq"
	case NotEq:
		data = "notEq"
	case Like:
		data = "like"
	case NotLike:
		data = "notLike"
	case GreaterThan:
		data = "greaterThan"
	case GreaterThanOrEq:
		data = "greaterThanOrEq"
	case LesserThan:
		data = "lesserThan"
	case LesserThanOrEq:
		data = "lesserThanOrEq"
	case IsNull:
		data = "isNull"
	case NotNull:
		data = "notNull"

	default:
		return nil, fmt.Errorf("Unable to marshal `%v` in to bytes", o)
	}
	return []byte(data), nil
}

// UnmarshalText implements https://golang.org/pkg/encoding/#TextUnmarshaler
func (o *Operator) UnmarshalText(b []byte) error {
	str := strings.Trim(string(b), `"`)

	switch str {
	case "eq":
		*o = Eq
	case "notEq":
		*o = NotEq
	case "like":
		*o = Like
	case "notLike":
		*o = NotLike
	case "greaterThan":
		*o = GreaterThan
	case "greaterThanOrEq":
		*o = GreaterThanOrEq
	case "lesserThan":
		*o = LesserThan
	case "lesserThanOrEq":
		*o = LesserThanOrEq
	case "isNull":
		*o = IsNull
	case "notNull":
		*o = NotNull

	default:
		return fmt.Errorf("Unable to marshal '%s' into type Operator", str)
	}
	return nil
}

// MarshalText implements https://golang.org/pkg/encoding/#TextMarshaler
func (c Condition) MarshalText() ([]byte, error) {
	var data string

	switch c {
	case And:
		data = "and"
	case Or:
		data = "or"

	default:
		return nil, fmt.Errorf("Unable to marshal `%v` in to bytes", c)
	}
	return []byte(data), nil
}

// UnmarshalText implements https://golang.org/pkg/encoding/#TextUnmarshaler
func (c *Condition) UnmarshalText(b []byte) error {
	str := strings.Trim(string(b), `"`)

	switch str {
	case "and":
		*c = And
	case "or":
		*c = Or

	default:
		return fmt.Errorf("Unable to marshal '%s' into type Condition", str)
	}
	return nil
}
