package {{.PackageName}}

// GENERATED CODE, EDITS WILL BE LOST
//
// generated from {{.PackageName}}/{{.TypeName}}
// using http://gitlab.com/brianstarke/go-beget

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	sq "github.com/lann/squirrel"
)

// Delete deletes a {{.TypeName}} from the database
func (u *{{.TypeName}}) Delete(db *sql.DB) error {
	query := sq.Delete("{{.TableName}}").
		Where("id = ?", u.ID).
		RunWith(db).
		PlaceholderFormat(sq.Dollar)

	_, err := query.Exec()

	return err
}

// {{.TypeName}}UpdateRequest defines a set of parameters for
// updating {{.TypeName}}.  It can be serialized and passed
// between services as JSON, or used to generate a SQL statement.
type {{.TypeName}}UpdateRequest struct {
	ID int64 `json:"id"`
	Updates map[{{.TypeName}}Field]interface{} `json:"updates"`
}

// New{{.TypeName}}UpdateRequest creates a new update request for Thing
func (u *{{.TypeName}}UpdateRequest) New{{.TypeName}}UpdateRequest(id int64) *{{.TypeName}}UpdateRequest {
	return &{{.TypeName}}UpdateRequest{
		ID: id,
	}
}

// AddUpdate is a convenience method for adding updates to this request.  Chainable.
func (u *{{.TypeName}}UpdateRequest) AddUpdate(field {{.TypeName}}Field, value interface{}) *{{.TypeName}}UpdateRequest {
	u.Updates[field] = value

	return u
}

// GenerateUpdateSQL returns the SQL string and placeholder values, or an error
func (u *{{.TypeName}}UpdateRequest) GenerateUpdateSQL() (string, []interface{}, error) {
	sql := sq.
		StatementBuilder.
		PlaceholderFormat(sq.Dollar).
		Update("{{.TableName}}")

	for field, value := range u.Updates {
		sql = sql.Set(field.DbFieldName(), value)
	}

	sql = sql.Where(sq.Eq{"id": u.ID})

	return sql.ToSql()
}

// ExecuteUpdate will take a sql.DB connection and execute this update
func (u *{{.TypeName}}UpdateRequest) ExecuteUpdate(db *sql.DB) error {
	sql, values, err := u.GenerateUpdateSQL()

	if err != nil {
		return err
	}

	_, err = db.Exec(sql, values)

	return err
}

// New{{.TypeName}}UpdateHandlerFunc returns an HTTP handler func for
// {{.TypeName}}UpdateRequests. It returns 200 and the results
// on success, 404 if not a POST, 400 on bad JSON, 500 on any
// other error.
func New{{.TypeName}}UpdateHandlerFunc(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "POST" {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		b, err := ioutil.ReadAll(r.Body)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(w, "No update request in the request body")
			return
		}

		var ur {{.TypeName}}UpdateRequest
		err = json.Unmarshal(b, &ur)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, "Error deserializing update request: %s", err.Error())
			return
		}

		err = ur.ExecuteUpdate(db)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "Error executing update request: %s", err.Error())
			return
		}

		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "ok")

		return
	}
}

// MarshalText implements https://golang.org/pkg/encoding/#TextMarshaler
func (u {{.TypeName}}UpdateRequest) MarshalText() ([]byte, error) {
	stringified := make(map[string]interface{})

	for key, value := range u.Updates {
		s, err := key.MarshalText()

		if err != nil {
			return nil, err
		}

		stringified[string(s)] = value
	}

	result := map[string]interface{}{
		"updates": stringified,
		"id":      u.ID,
	}

	return json.Marshal(result)
}

// UnmarshalText implements https://golang.org/pkg/encoding/#TextUnmarshaler
func (u *{{.TypeName}}UpdateRequest) UnmarshalText(b []byte) error {
	var i map[string]interface{}

	err := json.Unmarshal(b, &i)

	if err != nil {
		return err
	}

	u.ID = i["id"].(int64)

	var j = make(map[{{.TypeName}}Field]interface{})

	for key, value := range i["updates"].(map[string]interface{}) {
		var p {{.TypeName}}Field
		err = p.UnmarshalText([]byte(key))

		if err != nil {
			return err
		}
		j[p] = value
	}

	u.Updates = j

	return nil
}
